#include "osgrid.h"
#include "math.h"
#include "stdio.h"

#ifdef OSGRID_TEST
#include "iostream"
#endif

namespace OSGrid {

    char grid[] = "SVSWSXSYSZTVTWSQSRSSSTSUTQTRSLSMSNSOSPTLTMSFSGSHSJSKTFTGSASBSCSDSETATBNVNWNXNYNZOVOWNQNRNSNTNUOQORNLNMNNNONPOLOMNFNGNHNJNKOFOGNANBNCNDNEOAOBHVHWHXHYHZJVJWHQHRHSHTHUJQJRHLHMHNHOHPJLJM";

    GridRef::GridRef():
        easting(0.0), northing(0.0),
        latitude(0.0), longitude(0.0), height(0.0),
        x(0.0), y(0.0), z(0.0),
        tx(WGS84toOSGB36tx), ty(WGS84toOSGB36ty),
        tz(WGS84toOSGB36tz), scale(WGS84toOSGB36s),
        rx(WGS84toOSGB36rx), ry(WGS84toOSGB36ry),
        rz(WGS84toOSGB36rz),
        N0(-100000.0), E0(+400000.0), F0(0.9996012717),
        phi0(49.0), lambda0(-2.0), 
        n(0.0), a(aAiry1830), b(bAiry1830), e2(0.0),
        aGPS(aWGS84), bGPS(bWGS84), e2GPS(0.0)

    {
        sprintf(ref, "Not yet");
        n = (a - b) / (a + b);
        e2 = (a * a - b * b) / a / a ;
        e2GPS = (aGPS * aGPS - bGPS * bGPS) / aGPS / aGPS ;
        phi0 *= M_PI / 180.0;
        lambda0 *= M_PI / 180.0;
    }

    void GridRef::convertRad(double latitude, double longitude, double height) {
        // First transform to cartesian
        toCartesian(latitude, longitude, height);
        // Next transform from GPS to OS
        transform(x, y, z);
        // Convert back to ellipsoid
        toEllipsoid(x, y, z, 1e-10);
        // Then convert from lat, long to grid
        toGrid(this->latitude, this->longitude);
        // Store string
        writeref();
    }

    void GridRef::convertDeg(double latitude, double longitude, double height) {
        latitude *= M_PI / 180.0;
        longitude *= M_PI / 180.0;
        convertRad(latitude, longitude, height);
    }

    void GridRef::writeref() {
        double east = fmod(easting, 100000.0) / 1000.0;
        double north = fmod(northing, 100000.0) / 1000.0;
        int esquare = (int) easting / 100000.0;
        int nsquare = (int) northing / 100000.0;
        char * c = &grid[2 * (7 * nsquare + esquare)];
        ref[0] = *c;
        ref[1] = *(c + 1);
        sprintf(&ref[2], " %04.1f, %04.1f", east, north);
    }

    void GridRef::transform(double x, double y, double z) {
        this->x = tx + (1 + scale) * x  -rz * y + ry * z;
        this->y = ty + rz * x + (1 + scale) * y - rx * z;
        this->z = tz - ry * x + rx * y + (1 + scale) * z;
    }

    void GridRef::toGrid(double latitude, double longitude) {
        this->latitude = latitude;
        this->longitude = longitude;
        double cphi = cos(latitude);
        double sphi = sin(latitude);
        double tphi = tan(latitude);
        // nu = a * F0 (1 - e^2 sin^2(phi))^-0.5
        double nu = 1 - e2 * sphi * sphi;
        nu = pow(nu, -0.5);
        nu = a * F0 * nu;
        // rho = a * F0(1-e^2)(1-e^2 sin^2(phi))^-1.5
        double rho = 1 - e2 * sphi * sphi;
        rho = pow(rho, -1.5);
        rho = a * F0 * (1 - e2) * rho;
        // eta^2 = (nu / rho) - 1
        double eta2 = nu / rho - 1.0;
        /*
        M = b * F0 * (
            (1 + n + 5/4*n^2 + 5/4*n^3)(phi - phi0)
            - (3*n * 3*n^2 + 21/8*n^3)sin(phi - phi0)cos(phi+phi0)
            + (15/8*n^2 + 15/8*n^3)sin(2(phi - phi0))cos(2(phi+phi0))
            - 35/24*n^3sin(3(phi-phi0))cos(3(phi+phi0))
        )
        */
        double M0 = (1.0 + n + 5.0 / 4.0 * (n * n + n * n * n)) * (latitude - phi0);
        double M1 = 3.0 * n + 3.0 * n * n + 21.0 / 8.0 * n * n *n;
        M1 *= sin(latitude - phi0) * cos(latitude + phi0);
        double M2 = 15.0 / 8.0 * (n * n + n * n * n);
        M2 *= sin(2 * (latitude - phi0)) * cos (2 * (latitude + phi0));
        double M3 = 35.0 / 24.0 * n * n * n;
        M3 *= sin(3 * (latitude - phi0)) * cos( 3 * (latitude + phi0));
        double M = b * F0 * (M0 - M1 + M2 - M3);
        // x1 = M + N0
        double x1 = M + N0;
        // x2 = nu / 2 sin(phi) cos(phi)
        double x2 = nu / 2.0 * sphi * cphi;
        // x3 = nu / 24 * sin(phi) * cos^3(phi) * (5 - tan^2(phi) + 9 * eta^2)
        double x3 = nu / 24.0 * sphi * cphi * cphi * cphi;
        x3 *= 5.0 - tphi * tphi + 9 * eta2;
        // x3A =  nu / 720 * sin(phi) * cos^5(phi) * (61 - 58 * tan^2 (phi) + tan^4(phi))
        double x3a = nu / 720 * sphi * cphi * cphi * cphi * cphi * cphi;
        x3a *= 61.0 - 58.0 * tphi * tphi + tphi * tphi * tphi * tphi;
        // x4 = nu * cos(phi)
        double x4 = nu * cphi;
        // x5 = nu / 6 * cos^3(phi) * (nu / rho - tan^2(phi))
        double x5 = nu / 6.0 * cphi * cphi * cphi;
        x5 *= (nu / rho - tphi * tphi);
        // x6 = nu / 120 * cos^5(phi) * (5 - 18 * tan^2(phi) + tan^4(phi) + 14 * eta^2 - 58 * tan^2(phi) * eta^2)
        double x6 = 5.0 - 18.0 * tphi * tphi;
        x6 += tphi * tphi * tphi * tphi + 14.0 * eta2;
        x6 -= 58.0 * tphi * tphi * eta2;
        x6 *= nu / 120.0 * cphi * cphi * cphi * cphi * cphi;
        // N = x1 + x2*(longitude - lambda0)^2 + x3 * (longitude - lambda0)^4 + x3A * (longitude - lambda0)^6;
        double dlambda = longitude - lambda0;
        double dlambda2 = dlambda * dlambda;
        northing = x1 + x2 * dlambda2;
        northing += x3 * dlambda2 * dlambda2;
        northing += x3a * dlambda2 * dlambda2 * dlambda2;
        // E = E0 + x4 * (longitude - lamdba0) + x5 * (longitude - lambda0)^3 + x6 * (longitude - labda0)^5;
        easting = E0 + x4 * dlambda;
        easting += x5 * dlambda * dlambda2;
        easting += x6 * dlambda * dlambda2 * dlambda2;
    }

    void GridRef::toCartesian(double latitude, double longitude, double height) {
        this->latitude = latitude;
        this->longitude = longitude;
        this->height = height;
        double sphi = sin(latitude);
        double cphi = cos(latitude);
        double nu = aGPS / pow(1.0 - e2GPS * sphi * sphi, 0.5);
        x = (nu + height) * cphi * cos(longitude);
        y = (nu + height) * cphi * sin(longitude);
        z = ((1 - e2GPS) * nu + height) * sphi;
    }

    void GridRef::toEllipsoid(double x, double y, double z, double precision) {
        this->x = x;
        this->y = y;
        this->z = z;
        longitude = atan2(y, x);
        double p = pow(x * x + y * y, 0.5);
        latitude = atan2(z, p * (1 - e2));
        float sphi = sin(latitude);
        double nu = 0.0; 
        double previous = latitude + 2 * precision;
        //double diff = std::abs(latitude - previous);
        double diff = latitude - previous;
        if (diff < 0.0) diff *= -1.0;
        while (diff > precision) {
            previous = latitude;
            sphi = sin(latitude);
            nu = a / pow(1 - e2 * sphi * sphi, 0.5); 
            latitude = atan2(z + e2 * nu * sphi, p);
            //diff = std::abs(latitude - previous);
            diff = latitude - previous;
            if (diff < 0.0) diff *= -1.0;
        }
        sphi = sin(latitude);
        nu = a / pow(1 - e2 * sphi * sphi, 0.5); 
        height = p / cos(latitude) - nu;
    }

#ifdef OSGRID_TEST
    bool GridRef::test() {

        std::cout << "Running tests from https://www.ordnancesurvey.co.uk/";
        std::cout << "docs/support/guide-coordinate-systems-great-britain.pdf" << std::endl;
        std::cout << "Test lat, long to grid (from annex C.1):" << std::endl;
        // latitude = 53deg 39min 27.2531;// Convert to rad
        latitude = 52.0 + 39.0 / 60.0 + 27.2531 / 60.0 / 60.0;
        latitude *= M_PI / 180.0;
        // longitude = 001deg 43min 04.5177;// Convert to rad
        longitude = 1.0 + 43.0 / 60.0 + 4.5177 / 60.0 / 60.0;
        longitude *= M_PI / 180.0;
        toGrid(latitude, longitude);
        double Eexp = 651409.903; // metres
        double Nexp = 313177.270; // metres
        double dE = easting - Eexp;
        double dN = northing - Nexp;
        std::cout << "Error = " << dE << ", " << dN << " m." << std::endl;
        if (dE > 0.01 || dE < -0.01) {
            std::cout << "Easting error too large (> 10 cm)" << std::endl;
            return false;
        }
        if (dN > 0.01 || dN < -0.01) {
            std::cout << "Northing error too large (> 10 cm)" << std::endl;
            return false;
        }
        return true;
    }
#endif
}
