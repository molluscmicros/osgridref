/* 
Ordanance Survey grid rerfernece library for use with mbed SDK

Copyright 2017 Nick Kolpin (nick.kolpin@gmail.com), Mollusc Micros Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
*/
#ifndef __OSGRID_H__
#define __OSGRID_H__

#define NOOSGRID_TEST

namespace OSGrid {
    // See section 6.6 of https://www.ordnancesurvey.co.uk/docs/support/guide-coordinate-systems-great-britain.pdf
    const double WGS84toOSGB36tx = -446.448; // metres
    const double WGS84toOSGB36ty = +125.157; // metres
    const double WGS84toOSGB36tz = -542.060; // metres
    const double WGS84toOSGB36s  = +0.0000204894; // dimensionless
    const double WGS84toOSGB36rx = -0.00000051; // radians
    const double WGS84toOSGB36ry = -0.000001197; // radians
    const double WGS84toOSGB36rz = -0.000004083; // radians

    const double aAiry1830 = 6377563.396; // metres
    const double bAiry1830 = 6356256.909; // metres

    const double aWGS84 = 6378137.000;
    const double bWGS84 = 6356752.3141;



    //! Ordnance Survey grid reference
    class GridRef{
        public:
            GridRef();
            /** Convert lat/long (in radians) and height given by GPS to grid reference
            * @param latitude: latitude in radians
            * @param longitude: latitude in radians
            * @returns nothing, use public code, easting and northing
            */
            void convertRad(double latitude, double longitude, double height);
            /** Convert lat/long (in degrees) and height given by GPS to grid reference
            * @param latitude in degrees 
            * @param longitude in degrees
            * @param height in metres
            * @returns nothing, use easting and northing or ref
            */
            void convertDeg(double latitude, double longitude, double height);
            /** String grid reference
            * @returns string with format 'CC EE.E, NN.N'
            * where the CC is the two char grid code and the
            * eastings 'EE.E' and northing 'NN.N are give in km.
            */
            char ref[16];
            //! Full easting and northing in metres
            double easting, northing;
#ifdef OSGRID_TEST
            bool test();
#endif
        private:
            void transform(double x, double y, double z);
            void toGrid(double latitude, double longitude);
            void toCartesian(double latitude, double longitude, double height);
            void toEllipsoid(double x, double y, double z, double precision);
            void writeref();
            double latitude, longitude, height; 
            double x, y, z;
            double tx, ty, tz, scale, rx, ry, rz;
            double N0, E0, F0, phi0, lambda0, n, a, b, e2;
            double aGPS, bGPS, e2GPS;
    };
}
#endif
