Ordnance Survey Grid References
===============================

This library converts longitude and latitude values given by GPS systems into the UK's Ordnance Survey easting and northing grid references.
This library implements conversions as described in https://www.ordnancesurvey.co.uk/docs/support/guide-coordinate-systems-great-britain.pdf
The precision of the conversion should be better than 10 metres, but approximations are used and so these values should not be used where better precision is required.

To install using the mbed-cli:

````
mbed add bitbucket.org/molluscmicros/osgridref
````

Example usage:
````
#include "mbed.h"

#include "osgrid.h"

int main() {
    OSGrid::GridReference gridref;
    // This is somewhere around Cahril, Oxfordshire
    // latitude: 51 deg, 29' N
    double latitude = 51.0 + 29.0 / 60.0; // degrees
    // longitude: 0 deg, 53' W
    double latitude = -53.0 / 60.0; // degrees
    gridref.convertDeg(latitude, longitude);
    printf("%f, %f -> %s\n", latitude, longitude, gridref.ref());
};
````

License
-------

Copyright 2017 Nick Kolpin (nick.kolpin@gmail.com), Mollusc Micros Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
